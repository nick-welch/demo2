#include <conio.h> //getch
#include <iostream> // cin/cout
using namespace std;

int Main() {

	int input;
	cout << "enter an integer: ";
	cin >> input;

	if (input % 2) cout << " is odd.\n";
	else cout << " is even.\n";

	//switch case
	//int i;

	//cout << "Enter a day of Christmas (1 - 12): ";
	//cin >> i;

	//switch (i)
	//{
	//case 12: cout << "Twelve Drummers Drumming\n"; // case fall-through
	//case 11: cout << "Eleven Pipers Piping\n";
	//case 10: cout << "Ten Lords a Leaping\n";
	//case  9: cout << "Nine Ladies Dancing\n";
	//case  8: cout << "Eight Maids a Milking\n";
	//case  7: cout << "Seven Swans a Swimming\n";
	//case  6: cout << "Six Geese a Laying\n";
	//case  5: cout << "Five Golden Rings\n";
	//case  4: cout << "Four Calling Birds\n";
	//case  3: cout << "Three French Hens\n";
	//case  2: cout << "Two Turtle Doves\n";
	//case  1: cout << "A Partridge in a Pear Tree\n"; break;
	//default: cout << i << "is not a valid day of Christmas.\n";
	//}

	

	//looping
	/*for (int i = 1; i <= 10; i++) {
		cout << i << "\n";
	}*/

	int input;
	cout << "enter an int 1-5";
	while (input < 1 || input > 5) {
		cout << "you're really bad at following directions";
		cin >> input;
	}

	cout << "congrats you're not an idiot.";

	

		_getch();
		return 0;
}